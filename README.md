### Radio Pi

This is the script that powers my 1930's TrueTone Internet Radio!  It is a little bit messy but it should give you an idea of how to get rotary encoders working with the Pi and well as how to create a custom interface using pygame!

You can read more about this project here: http://sanitizedinput.com/pi-radio/

![Pi Radio](http://sanitizedinput.com/static/images/pi-radio.jpg)