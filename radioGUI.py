#!/usr/bin/python3
import time
import subprocess
import textwrap
import math
import pygame
import os
import gaugette.rotary_encoder
import gaugette.switch
import gaugette.gpio
from RPi import GPIO


#Create logfile
if os.path.exists("/home/pi/scripts/radio/.radiolog.log"):
    append_write = 'a'
else:
    append_write = 'w'

logfile = open("/home/pi/scripts/radio/.radiolog.log", append_write)
logfile.write("Radio Started at {}\n".format(time.time()))

#get last values for tuner and volume
with open("/home/pi/scripts/radio/savestation.txt", "r") as infile:
    line = infile.readline()
    try:
        rotation = float(line)
    except:
        rotation = 0
with open("/home/pi/scripts/radio/savevolume.txt", "r") as invol:
    line = invol.readline()
    try:
        volume = float(line)
    except:
        volume = 60



#Tuner (rot encoder 2)
A_PIN = 0
B_PIN = 2
SW_PIN = 3

#Volume (rot encoder 3)
Av_PIN = 13
Bv_PIN = 12
SWv_PIN = 14

#Radio Selector (rot encoder 1)
Ars_PIN = 8
Brs_PIN = 7
SWrs_PIN = 9


#gadgett worker thread to read rotary encoders
gpio = gaugette.gpio.GPIO()
encoderv = gaugette.rotary_encoder.RotaryEncoder.Worker(gpio, Av_PIN, Bv_PIN)
encoder = gaugette.rotary_encoder.RotaryEncoder.Worker(gpio, A_PIN, B_PIN)
encoderrs = gaugette.rotary_encoder.RotaryEncoder.Worker(gpio, Ars_PIN, Brs_PIN)
encoderv.start()
encoder.start()
encoderrs.start()

#Tuner switch (displays station info)
switch = gaugette.switch.Switch(gpio, SW_PIN)
last_state = None

#Mute button on volume knob
muteswitch = gaugette.switch.Switch(gpio, SWv_PIN)
SWv_state = False

#Shutdown button on Radio Selector knob
shutswitch = gaugette.switch.Switch(gpio, SWrs_PIN)
SWrs_state = False


#Change these to move where stations are on the dial
s1a = range(0, 4)
s1b = range(357, 360)
s2 = range(30, 35)
s3 = range(60, 65)
s4 = range(86, 91)
s5 = range(114, 121)
s6 = range(144, 149)
s7 = range(179, 184)
s8 = range(213, 218)
s9 = range(244, 249)
s10 = range(271, 276)
s11 = range(298, 303)
s12 = range(327, 332)

# initialize game engine
pygame.init()
pygame.font.init()

window_width=974
window_height=1092
animation_increment=10
clock_tick_rate=20
station = False
dead=False

# Open a window
size = (window_width, window_height)
screen = pygame.display.set_mode((700, 1024), pygame.FULLSCREEN)
pygame.display.set_caption("Pi Radio")
clock = pygame.time.Clock()
background_image = pygame.image.load("/home/pi/scripts/radio/radiobg3.png").convert()
image_surf = pygame.image.load("/home/pi/scripts/radio/handP.png").convert()
image_surf.set_colorkey((255,0,255))
imagerect = image_surf.get_rect()
blittedRect = screen.blit(image_surf, (330, 330))
pygame.mouse.set_visible(0)
pygame.display.flip()
laststate = 'up'

#set some initial values
radioList = ["Oldy", "Cyberpunk", "Indie", "Aria"]
radioListCounter = 0
subprocess.call("mpc single on", shell=True)
subprocess.call("mpc repeat on", shell=True)
subprocess.call("mpc play 13", shell=True)
subprocess.call("mpc clear", shell=True)
subprocess.call("mpc load oldy", shell=True)


def drawText(surface, text, color, rect, font, aa=False, bkg=None):
    '''
    Takes text and wordwraps it as separate images.  Returns image list.
    '''
    rect = pygame.Rect(rect)
    y = rect.top
    lineSpacing = -2

    # get the height of the font
    fontHeight = font.size("Tg")[1]
    imagelist = []
    while text:
        i = 1

        # determine if the row of text will be outside our area
        if y + fontHeight > rect.bottom:
            break

        # determine maximum width of line
        while font.size(text[:i])[0] < rect.width and i < len(text):
            i += 1

        # if we've wrapped the text, then adjust the wrap to the last word
        if i < len(text):
            i = text.rfind(" ", 0, i) + 1

        # render the line and blit it to the surface
        if bkg:
            image = font.render(text[:i], 1, color, bkg)
            image.set_colorkey(bkg)
        else:
            image = font.render(text[:i], aa, color)

        y += fontHeight + lineSpacing

        # remove the text we just blitted
        text = text[i:]
        imagelist.append(image)
    return imagelist, y


def currentSong():
    '''
    Gets the current station and song title and returns it as a string
    have to do some funky stuff to try and parse the different ways that some
    stations return their data
    '''
    outstring = subprocess.check_output(["mpc", "current"])
    outstring = outstring.decode("utf-8")
    if "text=" in outstring:
        try:
            outstring_list = outstring.split(" - ")
            artist= outstring_list[0]
            song = outstring_list[1].split("\"")
            song = song = song[1]
            returnstring = "{}: {}".format(artist, song)
        except Exception as e:
            logfile.write("Exception while parsing iheartradio station: {}\n".format(e))
            returnstring = outstring
    else:
        outlist = outstring.split(":")
        try:
            station = outlist[0]
            program = outlist[1]
            returnstring = "{}:    {}".format(station, program)
        except Exception as e:
            logfile.write("Exception while parsing typical station: {}\n".format(e))
            returnstring = outstring
    returnstring = returnstring.rstrip()
    return returnstring


def playStation(stationNumber):
    '''
    Plays the given station
    '''
    try:
        subprocess.check_output(["mpc", "play", str(stationNumber)])
    except Exception as e:
        logfile.write("Exception while trying to play {}: {}\n".format(stationNumber, e))

activeFont = "/home/pi/scripts/radio/bettynoir.ttf"
fontColor = (235,187,90)
program = "N/A"

last_time = mlast_time = station_time = time.time()
olddeltars = 0
radiocounter = 0
shutdownPi = False
OldVolume = 50


#PyGame loop starts here
while(dead == False):
    #Periodically check the song title and save it to program variable
    now = time.time()
    diff = now - last_time
    mdiff = now - mlast_time
    stationDiff = now - station_time
    if diff > 3:
        program = currentSong()
        last_time = time.time()

    #Read volume
    deltav = encoderv.get_steps()
    if deltav!=0:
        volume = volume + (deltav/1.5)
        volume = int(round(volume))
        if volume < 0:
            volume = 0
        elif volume > 100:
            volume = 100
        subprocess.call("mpc volume {}".format(volume), shell=True)

    #Read radio Selector
    deltars = encoderrs.get_steps()
    if deltars !=0 and stationDiff > .15:
        radios = len(radioList)
        if deltars >= 1:
            dincr = 1
        elif deltars < 0:
            dincr = -1
        radioListCounter = (radioListCounter) + dincr
        #logfile.write("Pre radioListCounter: {}, Station: {}\n".format(radioListCounter, radioList[radioListCounter % len(radioList)]))

        #figure out which radio we are supposed to be using.  For new 'radios'
        #just add another elif and copy the structure of one of the existing ones
        #You must also add the new radio name to the radioList
        if radioList[radioListCounter % len(radioList)] == "Aria" and stationDiff > .15:
            background_image = pygame.image.load("/home/pi/scripts/radio/radiobgAri.png").convert()
            activeFont = "/home/pi/scripts/radio/PonyvilleMedium.ttf"
            image_surf = pygame.image.load("/home/pi/scripts/radio/handP.png").convert()
            image_surf.set_colorkey((255,0,255))
            subprocess.call("mpc clear", shell=True)
            subprocess.call("mpc load aria1", shell=True)
            subprocess.call("mpc stop", shell=True)
            playStation(currentStation)
            fontColor = (237, 128, 166)
            station_time = time.time()
        elif radioList[radioListCounter % len(radioList)] == "Oldy" and stationDiff > .15:
            background_image = pygame.image.load("/home/pi/scripts/radio/radiobg3.png").convert()
            activeFont = "/home/pi/scripts/radio/bettynoir.ttf"
            image_surf = pygame.image.load("/home/pi/scripts/radio/handP.png").convert()
            image_surf.set_colorkey((255,0,255))
            fontColor = (235,187,90)
            subprocess.call("mpc clear", shell=True)
            subprocess.call("mpc load oldy", shell=True)
            subprocess.call("mpc stop", shell=True)
            playStation(currentStation)
            station_time = time.time()
        elif radioList[radioListCounter % len(radioList)] == "Cyberpunk" and stationDiff > .15:
            background_image = pygame.image.load("/home/pi/scripts/radio/cyberpunkbg.png").convert()
            activeFont = "/home/pi/scripts/radio/cyberpunk-revival.ttf"
            image_surf = pygame.image.load("/home/pi/scripts/radio/retrosynth_hand.png").convert()
            image_surf.set_colorkey((255,0,255))
            fontColor = (255,0,128)
            subprocess.call("mpc clear", shell=True)
            subprocess.call("mpc load retrosynth1", shell=True)
            subprocess.call("mpc stop", shell=True)
            playStation(currentStation)
            station_time = time.time()
        elif radioList[radioListCounter % len(radioList)] == "Indie" and stationDiff > .15:
            background_image = pygame.image.load("/home/pi/scripts/radio/indiebg.png").convert()
            activeFont = "/home/pi/scripts/radio/cyberpunk-revival.ttf"
            image_surf = pygame.image.load("/home/pi/scripts/radio/retrosynth_hand.png").convert()
            image_surf.set_colorkey((255,0,255))
            fontColor = (255,0,128)
            subprocess.call("mpc clear", shell=True)
            subprocess.call("mpc load indie", shell=True)
            subprocess.call("mpc stop", shell=True)
            playStation(currentStation)
            station_time = time.time()

    #read tuner
    delta = encoder.get_steps()
    if delta != 0:
        rotation = rotation + (delta/1.5)
        rotation = int(round(rotation))
        if rotation < 0:
            rotation = rotation + 360
        elif rotation > 359:
            rotation = rotation - 360

    screen.blit(background_image, [27, 330])
    pygame.time.wait(0)

    #quit if window is closed or escape is pressed
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            dead = True
    if event.type == pygame.KEYDOWN:
        if event.key == pygame.K_ESCAPE:
            dead = True

    #Draw tuner needle based on tuner rotary encoder value
    oldCenter = blittedRect.center
    rotatedSurf = pygame.transform.rotate(image_surf,rotation)
    rotatedSurf.set_colorkey((255,0,255))
    rotatedSurf.convert_alpha()
    rotRect = rotatedSurf.get_rect()
    rotRect.center = oldCenter
    screen.blit(rotatedSurf, rotRect)

    #Play station or static based on where tuner needle is pointing
    if all(rotation not in i for i in (s1a,s1b,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12)) and station == True:
        subprocess.call("mpc stop", shell=True)
        subprocess.call("mpc play 13", shell=True)
        station = False
        currentStation = 0
    else:
        if (rotation in s1a and station == False) or (rotation in s1b and station == False):
            subprocess.call("mpc stop", shell=True)
            playStation(1)
            station = True
            currentStation = 1
        if rotation in s2 and station == False:
            subprocess.call("mpc stop", shell=True)
            playStation(2)
            station = True
            currentStation = 2
        if rotation in s3 and station == False:
            subprocess.call("mpc stop", shell=True)
            playStation(3)
            station = True
            currentStation = 3
        if rotation in s4 and station == False:
            subprocess.call("mpc stop", shell=True)
            playStation(4)
            station = True
            currentStation = 4
        if rotation in s5 and station == False:
            subprocess.call("mpc stop", shell=True)
            playStation(5)
            station = True
            currentStation = 5
        if rotation in s6 and station == False:
            subprocess.call("mpc stop", shell=True)
            playStation(6)
            station = True
            currentStation = 6
        if rotation in s7 and station == False:
            subprocess.call("mpc stop", shell=True)
            playStation(7)
            station = True
            currentStation = 7
        if rotation in s8 and station == False:
            subprocess.call("mpc stop", shell=True)
            playStation(8)
            station = True
            currentStation = 8
        if rotation in s9 and station == False:
            subprocess.call("mpc stop", shell=True)
            playStation(9)
            station = True
            currentStation = 9
        if rotation in s10 and station == False:
            subprocess.call("mpc stop", shell=True)
            playStation(10)
            station = True
            currentStation = 10
        if rotation in s11 and station == False:
            subprocess.call("mpc stop", shell=True)
            playStation(11)
            station = True
            currentStation = 11
        if rotation in s12 and station == False:
            subprocess.call("mpc stop", shell=True)
            playStation(12)
            station = True
            currentStation = 12

    #Uncomment if you need to see rotation value (Useful when setting station ranges)
    # basicfont = pygame.font.SysFont(None, 48)
    # text = basicfont.render(str(rotation), True, (255, 0, 0), (255, 255, 255))
    # textrect = text.get_rect()
    # textrect.centerx = screen.get_rect().centerx
    # textrect.centery = screen.get_rect().centery
    # screen.blit(text, textrect)

    #Read tuner button state and display station info if pressed
    sw_state = switch.get_state()
    if sw_state == 0:
        pass
    elif sw_state == 1:
        font = pygame.font.Font(activeFont, 48)
        program = program.rstrip()
        textlist, y = drawText(screen, program, fontColor, (100,100,500,600), font, False, (100,50,250))
        offset = 0
        screen.fill((0,0,0))
        for text in textlist:
            offset = offset + 48
            textrect = text.get_rect()
            textrect.centerx = screen.get_rect().centerx
            textrect.centery = screen.get_rect().centery
            textrect.centery = textrect.centery + offset
            screen.blit(text, textrect)

    #Read mute button and see if we need to toggle mute on/offset.  Has a 1 second
    #Hold down timer to keep it from toggling too fast.
    mute_state = muteswitch.get_state()
    if mute_state == 1:
        if mdiff > 1:
            if SWv_state == False:
                if volume != 0:
                    OldVolume = volume
                volume = 0
                subprocess.call("mpc volume {}".format(volume), shell=True)
                SWv_state = True
            elif SWv_state == True:
                volume = OldVolume
                subprocess.call("mpc volume {}".format(volume), shell=True)
                SWv_state = False
            mlast_time = time.time()
    pygame.display.flip()

    #Read shutdown button, if pressed cleanup program, exit, and shutdown the Pi
    shutdown_state = shutswitch.get_state()
    if shutdown_state == 1:
        dead = True
        shutdownPi = True

#cleanup GPIO and stop playing on exit
GPIO.cleanup()
subprocess.call("mpc stop", shell=True)
logfile.close()


#Save volume value and tuner value on exit
with open("/home/pi/scripts/radio/savestation.txt", "w") as outfile:
    outfile.write(str(rotation))
with open("/home/pi/scripts/radio/savevolume.txt", "w") as outvol:
    outvol.write(str(volume))

if shutdownPi == True:
    subprocess.call("sudo shutdown -h now", shell=True)
